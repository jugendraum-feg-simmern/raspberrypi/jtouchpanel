/*!
 * \file einstellung_theke.cpp
 * \brief Source für die Theke-Einstellung GUI
 */
#include "inc/einstellung_theke.h"
#include "ui_einstellung_theke.h"

EinstellungTheke::EinstellungTheke(QWidget *parent, JugendraumTCPClient *j) :
    QDialog(parent),
    ui_(new Ui::EinstellungTheke)
{
    qDebug() << Q_FUNC_INFO;
        
    api_mgr_ = new APIParameterManager(this, j);
    api_mgr_->addAPIParameter("ThekeDeckenlichtOn", &EinstellungTheke::onDeckenlichtOnChanged);
    api_mgr_->addAPIParameter("ThekeRGBOn", &EinstellungTheke::onRGBOnChanged);
    api_mgr_->addAPIParameter("ThekeRGBRedValue", &EinstellungTheke::onRGBRedValueChanged);
    api_mgr_->addAPIParameter("ThekeRGBGreenValue", &EinstellungTheke::onRGBGreenValueChanged);
    api_mgr_->addAPIParameter("ThekeRGBBlueValue", &EinstellungTheke::onRGBBlueValueChanged);
    api_mgr_->addAPIParameter("ThekeRGBValue", &EinstellungTheke::onRGBChanged);
    api_mgr_->requestAll();

    // create GUI
    ui_->setupUi(this);
    ui_->horizontalLayoutWidget_2->setStyleSheet("border:none;");
    ui_->horizontalLayoutWidget_3->setStyleSheet("border:none;");
    this->setModal(true);
    this->setWindowFlag(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->show();

    // slide-in animation
//    QPropertyAnimation *animation = new QPropertyAnimation(this, "geometry");
//    animation->setDuration(250);
//    animation->setStartValue(QRect(-400,0,0,480));
//    animation->setEndValue(QRect(0,0,400,480));
//    animation->setEasingCurve(QEasingCurve::InExpo);
//    animation->start(QAbstractAnimation::DeleteWhenStopped);
}

EinstellungTheke::~EinstellungTheke()
{
    qDebug() << Q_FUNC_INFO;

    // close sliders if open
    ui_->pushButton_rgb_set->setChecked(false);
    delete ui_;
}

void EinstellungTheke::on_pushButton_back_released()
{
    qDebug() << Q_FUNC_INFO;  
    
    // slide-out Animation
//    QPropertyAnimation *animation = new QPropertyAnimation(this, "geometry");
//    connect(animation, SIGNAL(finished()), this, SLOT(close()));
//    animation->setDuration(150);
//    animation->setStartValue(QRect(0,0,400,480));
//    animation->setEndValue(QRect(-400,0,0,480));
//    animation->setEasingCurve(QEasingCurve::OutExpo);
//    animation->start(QAbstractAnimation::DeleteWhenStopped);
}

void EinstellungTheke::on_pushButton_lampen_on_off_toggled(bool checked)
{
    api_mgr_->change("ThekeDeckenlichtOn", checked);
}

void EinstellungTheke::on_pushButton_rgb_set_toggled(bool checked)
{
    qDebug() << Q_FUNC_INFO;

    if (checked)
    {
        // activate rgb, so that the changes take immediate effect
        ui_->pushButton_rgb_on_off->setChecked(true);

        // create the sliders
        if (not hw::sliderless())
        {
            slider_red_ =   new JSlider(this, 2, 0);
            slider_green_ = new JSlider(this, 3, 0);
            slider_blue_ =  new JSlider(this, 4, 0);
            connect(slider_red_, SIGNAL(changed(int)), this, SLOT(sliderRedChanged(int)));
            connect(slider_green_, SIGNAL(changed(int)), this, SLOT(sliderGreenChanged(int)));
            connect(slider_blue_, SIGNAL(changed(int)), this, SLOT(sliderBlueChanged(int)));
        }
    }
    else
    {
        if (not hw::sliderless())
        {
            // delete the sliders
            disconnect(slider_red_, SIGNAL(changed(int)), this, SLOT(sliderRedChanged(int)));
            disconnect(slider_green_, SIGNAL(changed(int)), this, SLOT(sliderGreenChanged(int)));
            disconnect(slider_blue_, SIGNAL(changed(int)), this, SLOT(sliderBlueChanged(int)));
            delete slider_red_;
            delete slider_green_;
            delete slider_blue_;
        }
    }

}

void EinstellungTheke::on_pushButton_rgb_on_off_toggled(bool checked)
{
    api_mgr_->change("ThekeRGBOn", checked);

    // close sliders if rgb gets switched off
    if (not checked) ui_->pushButton_rgb_set->setChecked(false);
}

void EinstellungTheke::sliderRedChanged(int val)
{
    qDebug() << Q_FUNC_INFO;
    api_mgr_->change("ThekeRGBRedValue", val);
}

void EinstellungTheke::sliderGreenChanged(int val)
{
    qDebug() << Q_FUNC_INFO;
    api_mgr_->change("ThekeRGBGreenValue", val);
}

void EinstellungTheke::sliderBlueChanged(int val)
{
    qDebug() << Q_FUNC_INFO;
    api_mgr_->change("ThekeRGBBlueValue", val);
}

void EinstellungTheke::onDeckenlichtOnChanged(bool state)
{
    ui_->pushButton_lampen_on_off->setChecked(state);
    ui_->pushButton_lampen_on_off->setText(state ? "Aus" : "An");
}

void EinstellungTheke::onRGBOnChanged(bool state)
{
    ui_->pushButton_rgb_on_off->setChecked(state);
    ui_->pushButton_rgb_on_off->setText(state ? "Aus" : "An");
}

void EinstellungTheke::onRGBRedValueChanged(int val)
{
    ui_->test_label_1->setText(QString("R: %1").arg(val));
    if (slider_red_ != nullptr)
        slider_red_->setPosition(val);
}

void EinstellungTheke::onRGBGreenValueChanged(int val)
{
    ui_->test_label_2->setText(QString("G: %1").arg(val));
    if (slider_green_ != nullptr)
        slider_green_->setPosition(val);
}

void EinstellungTheke::onRGBBlueValueChanged(int val)
{
    ui_->test_label_3->setText(QString("G: %1").arg(val));
    if (slider_blue_ != nullptr)
        slider_blue_->setPosition(val);
}

void EinstellungTheke::onRGBChanged(QList<int> rgb)
{
    onRGBRedValueChanged(rgb[0]);
    onRGBGreenValueChanged(rgb[1]);
    onRGBBlueValueChanged(rgb[2]);
}
