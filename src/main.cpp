/*!
 * \authors
 *      + [CPLMaddin](https://github.com/CPLMaddin)
 *      + [JonasWitzenrath](https://github.com/JonasWitzenrath)
 *      + [ChrisSeibel](https://github.com/ChrisSeibel)
 * \version 4.0
 *
 * \copyright GNU Public License
 *
 * \mainpage Die GUI zur Steuerung des Jugendraumes
 *
 */

#include "inc/application.h"

#include "inc/jugendraum_tcp_client.h"
#include "inc/startseite.h"


int main(int argc, char *argv[])
{
    Application a(argc, argv);
    JugendraumTCPClient jugendraum;
    Startseite startseite(nullptr, &jugendraum);
    //startseite.setWindowFlag(Qt::FramelessWindowHint);
    startseite.showFullScreen();

    return a.exec();
}
