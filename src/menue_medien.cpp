/*!
 * \file menue_medien.cpp
 * \brief Header für das Medien Menü
 */
#include "inc/menue_medien.h"
#include "ui_menue_medien.h"

MenueMedien::MenueMedien(QWidget *parent, JugendraumTCPClient *j) :
    QDialog(parent),
    ui_(new Ui::MenueMedien)
{
    qDebug() << Q_FUNC_INFO;

    jugendraum_ = j;
    api_mgr_ = new APIParameterManager(this, j);
    api_mgr_->addAPIParameter("PCOn", &MenueMedien::onPCOnChanged);
    api_mgr_->requestAll();

    // erzeuge GUI
    ui_->setupUi(this);
    this->setModal(true);
    this->setWindowFlag(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->show();

    // slide-in Animation
    QPropertyAnimation *animation = new QPropertyAnimation(this, "geometry");
    animation->setDuration(250);
    animation->setStartValue(QRect(-400,0,400,480));
    animation->setEndValue(QRect(0,0,400,480));
    animation->setEasingCurve(QEasingCurve::InExpo);
    animation->start(QAbstractAnimation::DeleteWhenStopped);
}

MenueMedien::~MenueMedien()
{
    qDebug() << Q_FUNC_INFO;
    delete ui_;
}

void MenueMedien::on_pushButton_audio_released()
{
    qDebug() << Q_FUNC_INFO;
    
    einstellung_audio_ = new EinstellungAudio(this, jugendraum_);
}

void MenueMedien::on_pushButton_pc_released()
{
    api_mgr_->change("PCOn", !pc_is_on_);
}

void MenueMedien::on_pushButton_back_released()
{
    qDebug() << Q_FUNC_INFO;

    // slide-out Animation
    QPropertyAnimation *animation = new QPropertyAnimation(this, "geometry");
    connect(animation, SIGNAL(finished()), this, SLOT(close()));
    animation->setDuration(150);
    animation->setStartValue(QRect(0,0,400,480));
    animation->setEndValue(QRect(-400,0,400,480));
    animation->setEasingCurve(QEasingCurve::OutExpo);
    animation->start(QAbstractAnimation::DeleteWhenStopped);
}

void MenueMedien::onPCOnChanged(bool state)
{
    qDebug() << Q_FUNC_INFO;
    pc_is_on_ = state;
    // nothing to be done sice there is no visual indication of state of pc
    // maybe this should be changed?
    // there is a related issue on gitlab in the controlpanel repo
}
