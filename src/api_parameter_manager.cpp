#include "inc/api_parameter_manager.h"

APIParameterManager::APIParameterManager(QDialog *p, JugendraumTCPClient *j)
{
    parent_ = p;
    jugendraum_ = j;

    connect(jugendraum_, SIGNAL(APIParameterChanged(QString, QVariant)), this, SLOT(onAPIParameterChanged(QString, QVariant)));
}

APIParameterManager::~APIParameterManager()
{

}

void APIParameterManager::onAPIParameterChanged(QString param, QVariant val) {
    if (api_parameters_.contains(param))
        api_parameters_[param](val);
}

void APIParameterManager::request(QString param)
{
    jugendraum_->requestAPIParameter(param);
}

void APIParameterManager::change(QString param, QVariant val)
{
    jugendraum_->changeAPIParameter(param, val);
}

void APIParameterManager::requestAll()
{
    for (auto p : api_parameters_.keys())
        jugendraum_->requestAPIParameter(p);
}
