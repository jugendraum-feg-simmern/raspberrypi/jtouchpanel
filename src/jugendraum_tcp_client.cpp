#include "../inc/jugendraum_tcp_client.h"

JugendraumTCPClient::JugendraumTCPClient(QObject *parent, QHostAddress ip, quint16 port) : QObject(parent)
{
    sock_ = new QTcpSocket(this);
    sock_->connectToHost(ip, port);
    connect(sock_, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(sock_, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(onSocketStateChanged(QAbstractSocket::SocketState)));
}

JugendraumTCPClient::~JugendraumTCPClient()
{
    sock_->close();
    delete sock_;
}

int JugendraumTCPClient::requestAPIParameter(QString param, QVariant arg)
{
    if (not sock_->isWritable())
        return -1;

    QVariantList vl;
    vl << CMD_REQUEST_PARAM << param << arg;
    return sock_->write(MsgPack::pack(vl));
}

int JugendraumTCPClient::requestAPIParameter(QString param)
{
    if (not sock_->isWritable())
        return -1;

    QVariantList vl;
    vl << CMD_REQUEST_PARAM << param;
    return sock_->write(MsgPack::pack(vl));
}

int JugendraumTCPClient::changeAPIParameter(QString param, QVariant val)
{
    if (not sock_->isWritable())
        return -1;

    QVariantList vl;
    vl << CMD_CHANGE_PARAM << param << val;
    return sock_->write(MsgPack::pack(vl));
}

void JugendraumTCPClient::onSocketStateChanged(QAbstractSocket::SocketState state)
{
    qDebug() << "Socket changed to " << state;
}

void JugendraumTCPClient::onReadyRead()
{
    QByteArray data_raw = sock_->readAll();
    QVariant v = MsgPack::unpack(data_raw);
    qDebug() << v;
}
