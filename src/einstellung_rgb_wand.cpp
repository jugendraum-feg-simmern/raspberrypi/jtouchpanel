/*!
 * \file einstellung_rgb_wand.cpp
 * \brief Source für die Wand-RGB-Einstellung GUI
 */
#include "inc/einstellung_rgb_wand.h"
#include "ui_einstellung_rgb_wand.h"

EinstellungRGBWand::EinstellungRGBWand(QWidget *parent, JugendraumTCPClient *j) :
    QDialog(parent),
    ui_(new Ui::EinstellungRGBWand)
{
    qDebug() << Q_FUNC_INFO;
        
    // create api manager
    api_mgr_ = new APIParameterManager(this, j);
    api_mgr_->addAPIParameter("WandRGBOn", &EinstellungRGBWand::onRGBOnChanged);
    api_mgr_->addAPIParameter("WandRGBRedValue", &EinstellungRGBWand::onRGBRedValueChanged);
    api_mgr_->addAPIParameter("WandRGBGreenValue", &EinstellungRGBWand::onRGBGreenValueChanged);
    api_mgr_->addAPIParameter("WandRGBBlueValue", &EinstellungRGBWand::onRGBBlueValueChanged);
    api_mgr_->addAPIParameter("WandRGBValue", &EinstellungRGBWand::onRGBChanged);
    api_mgr_->requestAll();


    // create GUI
    ui_->setupUi(this);
    ui_->horizontalLayoutWidget->setStyleSheet("border:none;");
    this->setModal(true);
    this->setWindowFlag(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->show();

    // slide-in Animation
//    QPropertyAnimation *animation = new QPropertyAnimation(this, "geometry");
//    animation->setDuration(250);
//    animation->setStartValue(QRect(-400,0,0,480));
//    animation->setEndValue(QRect(0,0,400,480));
//    animation->setEasingCurve(QEasingCurve::InExpo);
//    animation->start(QAbstractAnimation::DeleteWhenStopped);
}

EinstellungRGBWand::~EinstellungRGBWand()
{
    qDebug() << Q_FUNC_INFO;
    ui_->pushButton_set_color->setChecked(false);
    delete ui_;
}

void EinstellungRGBWand::on_pushButton_back_released()
{
    qDebug() << Q_FUNC_INFO;
    
    // slide-out Animation
//    QPropertyAnimation *animation = new QPropertyAnimation(this, "geometry");
//    connect(animation, SIGNAL(finished()), this, SLOT(close()));
//    animation->setDuration(150);
//    animation->setStartValue(QRect(0,0,400,480));
//    animation->setEndValue(QRect(-400,0,0,480));
//    animation->setEasingCurve(QEasingCurve::OutExpo);
//    animation->start(QAbstractAnimation::DeleteWhenStopped);
}

void EinstellungRGBWand::on_pushButton_on_off_toggled(bool checked)
{
    api_mgr_->change("WandRGBOn", checked);

    // close sliders if rgb gets switched off
    if (not checked) ui_->pushButton_set_color->setChecked(false);
}

void EinstellungRGBWand::on_pushButton_set_color_toggled(bool checked)
{
    qDebug() << Q_FUNC_INFO;

    if (checked)
    {
        // activate rgb, so that the changes take immediate effect
        ui_->pushButton_on_off->setChecked(true);

        if (not hw::sliderless())
        {
            // create the sliders
            slider_red_ =   new JSlider(this, 2, 0);
            slider_green_ = new JSlider(this, 3, 0);
            slider_blue_ =  new JSlider(this, 4, 0);
            connect(slider_red_, SIGNAL(changed(int)), this, SLOT(sliderRedChanged(int)));
            connect(slider_green_, SIGNAL(changed(int)), this, SLOT(sliderGreenChanged(int)));
            connect(slider_blue_, SIGNAL(changed(int)), this, SLOT(sliderBlueChanged(int)));
        }
    }
    else
    {
        if (not hw::sliderless())
        {
            // delete the sliders
            disconnect(slider_red_, SIGNAL(changed(int)), this, SLOT(sliderRedChanged(int)));
            disconnect(slider_green_, SIGNAL(changed(int)), this, SLOT(sliderGreenChanged(int)));
            disconnect(slider_blue_, SIGNAL(changed(int)), this, SLOT(sliderBlueChanged(int)));
            delete slider_red_;
            delete slider_green_;
            delete slider_blue_;
        }
    }
}

void EinstellungRGBWand::sliderRedChanged(int val)
{
    qDebug() << Q_FUNC_INFO;
    api_mgr_->change("WandRGBRedValue", val);
}

void EinstellungRGBWand::sliderGreenChanged(int val)
{
    qDebug() << Q_FUNC_INFO;
    api_mgr_->change("WandRGBGreenValue", val);
}

void EinstellungRGBWand::sliderBlueChanged(int val)
{
    qDebug() << Q_FUNC_INFO;
    api_mgr_->change("WandRGBBlueValue", val);
}

void EinstellungRGBWand::onRGBOnChanged(bool state)
{
    ui_->pushButton_on_off->setText(state ? "Aus" : "An");
}

void EinstellungRGBWand::onRGBRedValueChanged(int val)
{
    ui_->test_label_1->setText(QString("R: %1").arg(val));
    if (slider_red_ != nullptr)
        slider_red_->setPosition(val);
}

void EinstellungRGBWand::onRGBGreenValueChanged(int val)
{
    ui_->test_label_2->setText(QString("G: %1").arg(val));
    if (slider_green_ != nullptr)
        slider_green_->setPosition(val);
}

void EinstellungRGBWand::onRGBBlueValueChanged(int val)
{
    ui_->test_label_3->setText(QString("B: %1").arg(val));
    if (slider_blue_ != nullptr)
        slider_blue_->setPosition(val);
}

void EinstellungRGBWand::onRGBChanged(QList<int> rgb)
{
    onRGBRedValueChanged(rgb[0]);
    onRGBGreenValueChanged(rgb[1]);
    onRGBBlueValueChanged(rgb[2]);
}
