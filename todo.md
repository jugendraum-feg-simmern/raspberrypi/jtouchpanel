+ [ ] loadAllFromFile() in Startseite::on_pushButton_youth_released() = add parameter equivalent to Jugendraum::loadAllFromnFile()
+ [ ] loadAllFromFile() in Startseite::on_pushButton_teen_released() = add parameter equivalent to Jugendraum::loadAllFromnFile()
+ [ ] slide in animation in MenueLicht::MenueLicht()
+ [ ] slide in animation in MenueLicht::~MenueLicht()

+ [ ] slide out animation in EinstellungTheke::EinstellungTheke()
+ [ ] slide out animation in EinstellungTheke::~EinstellungTheke()
+ [ ] hw::sliderless in EinstellungTheke::on_push_button_rgb_set_toggled()

+ [ ] slide out animation in EinstellungRGBWand::EinstellungRGBWand()
+ [ ] slide out animation in EinstellungRGBWand::~EinstellungRGBWand()
+ [ ] hw::sliderless in EinstellungRGBWand::on_push_button_rgb_set_toggled()

+ [ ] slide out animation in EinstellungRGBDecke::EinstellungRGBDecke()
+ [ ] slide out animation in EinstellungRGBDecke::~EinstellungRGBDecke()
+ [ ] hw::sliderless in EinstellungRGBDecke::on_push_button_rgb_set_toggled()

+ [ ] slide out animation in EinstellungAudio::EinstellungAudio()
+ [ ] slide out animation in EinstellungAudio::~EinstellungAudio()
+ [ ] implement all functionality for EinstellungAudio

+ [ ] review implementation of reading temps in SystemStatus
