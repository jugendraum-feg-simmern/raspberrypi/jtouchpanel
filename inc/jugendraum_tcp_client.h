#ifndef JUGENDRAUMTCPCLIENT_H
#define JUGENDRAUMTCPCLIENT_H

#define CMD_REQUEST_PARAM 0
#define CMD_CHANGE_PARAM 1

#include <QObject>
#include <QHostAddress>
#include <QTcpSocket>

#include <qmsgpack/msgpack.h>

class JugendraumTCPClient : public QObject
{
    Q_OBJECT
public:
    explicit JugendraumTCPClient(QObject *parent = nullptr, QHostAddress ip = QHostAddress::LocalHost, quint16 port = 6330);
    ~JugendraumTCPClient();

    int requestAPIParameter(QString param);
    int requestAPIParameter(QString param, QVariant val);
    int changeAPIParameter(QString param, QVariant val);

private:
    QTcpSocket* sock_;

private slots:
    // tcp connection handler slots
    void onSocketStateChanged(QAbstractSocket::SocketState state);
    void onReadyRead();

signals:
    void APIParameterChanged(QString param, QVariant val);
};

#endif // JUGENDRAUMTCPCLIENT_H
