/*!
 * \file einstellung_theke.h 
 * \brief Header für die Theke-Einstellung GUI
 */
#ifndef EINSTELLUNG_THEKE_H
#define EINSTELLUNG_THEKE_H

#include <QDialog>
#include <QDebug>

#include "inc/jugendraum_tcp_client.h"
#include "inc/api_parameter_manager.h"
#include "inc/j_slider.h"

namespace Ui {
class EinstellungTheke;
}

/*!
 * \brief Klasse für die GUI der Thekeneinstellung
 * \todo Labels vom debuggen entfernen
 * \todo Hintergrund des Zurück-Knopfes wenn pressed
 */

class EinstellungTheke : public QDialog
{
    Q_OBJECT
    
public:
    explicit EinstellungTheke(QWidget *parent = nullptr, JugendraumTCPClient *j = nullptr);
    ~EinstellungTheke();
    
private slots:
    void on_pushButton_back_released();
    void on_pushButton_lampen_on_off_toggled(bool ckecked);
    void on_pushButton_rgb_set_toggled(bool checked);
    void on_pushButton_rgb_on_off_toggled(bool checked);
    void sliderRedChanged(int val);
    void sliderGreenChanged(int val);
    void sliderBlueChanged(int val);

    void onDeckenlichtOnChanged(bool state);
    void onRGBOnChanged(bool state);
    void onRGBRedValueChanged(int val);
    void onRGBGreenValueChanged(int val);
    void onRGBBlueValueChanged(int val);
    void onRGBChanged(QList<int> rgb);
    
private:
    Ui::EinstellungTheke* ui_;
    APIParameterManager* api_mgr_;
    JSlider* slider_red_ = nullptr, * slider_green_ = nullptr, * slider_blue_ = nullptr;
};

#endif // EINSTELLUNG_THEKE_H
