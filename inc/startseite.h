/*!
 * \file startseite.h
 * \brief Header für das Hauptfenster der GUI
 */
#ifndef STARTSEITE_H
#define STARTSEITE_H

#include <QMainWindow>
#include <QTimer>
#include <QDebug>
#include <QDateTime>

#include "inc/menue_licht.h"
#include "inc/menue_medien.h"
#include "inc/system_status.h"

#include "inc/jugendraum_tcp_client.h"

namespace Ui {
class Startseite;
}

/*!
 * \brief GUI: Startseite
 * \todo Laden der Jugend- / Teenkreis-Einstellungen
 */
class Startseite : public QMainWindow
{
    Q_OBJECT

public:
    explicit Startseite(QWidget *parent = nullptr, JugendraumTCPClient *j = nullptr);
    ~Startseite();

private slots:
    void on_pushButton_youth_released();
    void on_pushButton_teen_released();
    void on_pushButton_light_released();
    void on_pushButton_media_released();
    void on_pushButton_system_released();

    void updateDateTime();

private:
    Ui::Startseite *ui_;
    MenueLicht *menu_light_;
    MenueMedien *menu_media_;
    SystemStatus *system_status_;
    JugendraumTCPClient *jugendraum_;

    QTimer *clock_timer_;
};

#endif // STARTSEITE_H
