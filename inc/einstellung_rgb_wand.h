/*!
 * \file einstellung_rgb_wand.h 
 * \brief Header für die Wand-RGB-Einstellung GUI
 */
#ifndef EINSTELLUNGRGBWAND_H
#define EINSTELLUNGRGBWAND_H

#include <QDialog>
#include <QDebug>

#include "inc/jugendraum_tcp_client.h"
#include "inc/api_parameter_manager.h"
#include "inc/j_slider.h"

namespace Ui {
class EinstellungRGBWand;
}

/*!
 * \brief Klasse für GUI der Einstellungen der RGB an der Wand
 * \todo debug-labels in der GUI entfernen
 */

class EinstellungRGBWand : public QDialog
{
    Q_OBJECT
    
public:
    explicit EinstellungRGBWand(QWidget *parent = nullptr, JugendraumTCPClient *j = nullptr);
    ~EinstellungRGBWand();
    
private slots:
    void on_pushButton_back_released();
    void on_pushButton_on_off_toggled(bool checked);
    void on_pushButton_set_color_toggled(bool checked);
    void sliderRedChanged(int val);
    void sliderGreenChanged(int val);
    void sliderBlueChanged(int val);

    void onRGBOnChanged(bool state);
    void onRGBRedValueChanged(int val);
    void onRGBGreenValueChanged(int val);
    void onRGBBlueValueChanged(int val);
    void onRGBChanged(QList<int> rgb);

private:
    Ui::EinstellungRGBWand *ui_;
    APIParameterManager *api_mgr_;
    JSlider* slider_red_ = nullptr, * slider_green_ = nullptr, * slider_blue_ = nullptr;
};

#endif // EINSTELLUNGRGBWAND_H
