/*!
 * \file menue_licht.h
 * \brief Header für das Licht Menü
 */
#ifndef MENUE_LICHT_H
#define MENUE_LICHT_H

#include <QDateTime>
#include <QDebug>
#include <QDialog>
#include <QMap>
#include <QTimer>

#include "inc/jugendraum_tcp_client.h"
#include "inc/api_parameter_manager.h"

#include "inc/einstellung_hauptlicht.h"
#include "inc/einstellung_theke.h"
#include "inc/einstellung_rgb_wand.h"
#include "inc/einstellung_rgb_decke.h"
#include "inc/menue_speichern.h"


namespace Ui {
class MenueLicht;
}

/*!
 * \brief Klasse die Licht-Menü GUI
 * \todo Schließen button durch Icon ersetzen
 */
class MenueLicht : public QDialog
{
    Q_OBJECT

public:
    explicit MenueLicht(QWidget *parent = nullptr, JugendraumTCPClient *j = nullptr);
    ~MenueLicht();

public slots:

private slots:
    void on_pushButton_main_light_released();
    void on_pushButton_rgb_ceiling_released();
    void on_pushButton_counter_released();
    void on_pushButton_rgb_windows_released();
    void on_pushButton_palette_released();
    void on_pushButton_save_released();
    void on_pushButton_close_released();

    void updateTime();

    // handlerfunctions to update GUI
    void onPaletteStateChanged(bool state);

private:
    Ui::MenueLicht *ui_;
    JugendraumTCPClient *jugendraum_;
    APIParameterManager *api_mgr_;
    EinstellungTheke *einstellung_theke_;
    EinstellungRGBWand *einstellung_rgb_wand_;
    EinstellungRGBDecke *einstellung_rgb_decke_;
    EinstellungHauptlicht *einstellung_hauptlicht_;
    MenueSpeichern *menu_speichern_;

    QTimer *clock_timer_;

    bool palette_is_on_ = false;
};

#endif // MENUE_LICHT_H
