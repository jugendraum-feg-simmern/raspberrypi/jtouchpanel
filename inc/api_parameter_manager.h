#ifndef APIPARAMETERMANAGER_H
#define APIPARAMETERMANAGER_H

#include <QMap>
#include <QList>
#include <QString>
#include <QVariant>
#include <QDialog>
#include <QObject>

#include <functional>

#include "inc/jugendraum_tcp_client.h"


class APIParameterManager : public QObject
{
    Q_OBJECT
public:
    explicit APIParameterManager(QDialog* p, JugendraumTCPClient* j);
    ~APIParameterManager();
    template <class S, typename T>
    void addAPIParameter(QString param, void(S::*fptr)(T)) {
        api_parameters_.insert(param, [=](QVariant& arg){parent_->*fptr(arg.value<T>());});
    };
    void onAPIParameterChanged(QString param, QVariant val);
    void request(QString param);
    void change(QString param, QVariant val);
    void requestAll();

private:
    QDialog* parent_;
    JugendraumTCPClient* jugendraum_;
    QMap<QString, std::function<void(QVariant)>> api_parameters_;
};

#endif // APIPARAMETERMANAGER_H
