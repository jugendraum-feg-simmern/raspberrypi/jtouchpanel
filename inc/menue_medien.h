/*!
 * \file menue_medien.h
 * \brief Header für das Medien Menü
 */
#ifndef MENUE_MEDIEN_H
#define MENUE_MEDIEN_H

#include <QDialog>
#include <QTimer>
#include <QDateTime>
#include <QDebug>
#include <QTimer>


#include "inc/jugendraum_tcp_client.h"
#include "inc/api_parameter_manager.h"
#include "inc/hardware.h"

#include "inc/einstellung_audio.h"


namespace Ui {
class MenueMedien;
}

/*!
 * \brief Klasse für die Medien-Menü GUI
 */
class MenueMedien : public QDialog
{
    Q_OBJECT

public:
    explicit MenueMedien(QWidget *parent = nullptr, JugendraumTCPClient *j = nullptr);
    ~MenueMedien();

private slots:
    void on_pushButton_audio_released();
    void on_pushButton_pc_released();
    void on_pushButton_back_released();

    void onPCOnChanged(bool state);

private:
    Ui::MenueMedien *ui_;
    JugendraumTCPClient *jugendraum_;
    APIParameterManager* api_mgr_;
    EinstellungAudio *einstellung_audio_;

    bool pc_is_on_ = false;
};

#endif // MENUE_MEDIEN_H
