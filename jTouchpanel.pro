#    _ _____                _                            _
#   (_)_   _|__  _   _  ___| |__  _ __   __ _ _ __   ___| |      _ __  _ __ ___
#   | | | |/ _ \| | | |/ __| '_ \| '_ \ / _` | '_ \ / _ \ |     | '_ \| '__/ _ \
#   | | | | (_) | |_| | (__| | | | |_) | (_| | | | |  __/ |  _  | |_) | | | (_) |
#  _/ | |_|\___/ \__,_|\___|_| |_| .__/ \__,_|_| |_|\___|_| (_) | .__/|_|  \___/
# |__/                           |_|                            |_|

QT       += core gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jTouchpanel
TEMPLATE = app
CONFIG += c++11

# Emit warnigs when using deprecated features
DEFINES += QT_DEPRECATED_WARNINGS

# remove debug ouptut on release builds
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# rules for deployment.
target.path = /home/pi
INSTALLS += target

LIBS += -L/opt/qt5.10.1_rpi/sysroot/usr/local/lib -lwiringPi
INCLUDEPATH += /opt/qt5.10.1_rpi/sysroot/usr/local/include

# all files qt has to know about
SOURCES += \
    src/api_parameter_manager.cpp \
    src/einstellung_audio.cpp \
    src/einstellung_hauptlicht.cpp \
    src/einstellung_rgb_decke.cpp \
    src/einstellung_rgb_wand.cpp \
    src/einstellung_theke.cpp \
    src/jugendraum_tcp_client.cpp \
    src/menue_licht.cpp \
    src/menue_medien.cpp \
    src/menue_speichern.cpp \
    src/passwort.cpp \
    src/startseite.cpp \
    src/system_status.cpp \
    src/application.cpp \
    src/hardware.cpp \
    src/j_slider.cpp \
    src/log.cpp \
    src/main.cpp \

HEADERS += \
    inc/api_parameter_manager.h \
    inc/einstellung_audio.h \
    inc/einstellung_hauptlicht.h \
    inc/einstellung_rgb_decke.h \
    inc/einstellung_rgb_wand.h \
    inc/einstellung_theke.h \
    inc/jugendraum_tcp_client.h \
    inc/menue_licht.h \
    inc/menue_medien.h \
    inc/menue_speichern.h \
    inc/passwort.h \
    inc/startseite.h \
    inc/system_status.h \
    inc/application.h \
    inc/file_handler.h \
    inc/hardware.h \
    inc/hardware_config.h \
    inc/hardware_register.h \
    inc/j_slider.h \
    inc/log.h \

FORMS += \
    ui/einstellung_audio.ui \
    ui/einstellung_hauptlicht.ui \
    ui/einstellung_theke.ui \
    ui/einstellung_rgb_decke.ui \
    ui/einstellung_rgb_wand.ui \
    ui/menue_licht.ui \
    ui/menue_medien.ui \
    ui/menue_speichern.ui \
    ui/passwort.ui \
    ui/system_status.ui \
    ui/startseite.ui

RESOURCES += \
    ui/backgrounds.qrc \
    ui/icons.qrc

